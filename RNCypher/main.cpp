#include "../Encoder/Plugin.h"
#include "../Encoder/KeyRing.h"
#include <memory>
#include <vector>
#include <string>
#include <bitset>
#include <random>
#include <functional>

class __declspec(dllexport) RNCypherClass : public Plugin
{
private:
    void HandleData(KeyRing _kr, std::vector< std::bitset<8> >& _data, bool amEncrypting)
    {
        //We will now create an actual functional plugin
        if (_kr.GetEncryptionType() != GetPluginName())
        {
            //Need to somehow let host know it failed
            return;
        }
        else
        {
            //We begin by seeding the RNG for both
            srand(std::hash<std::string> {}(_kr.GetPassword2()));

            //DEV NOTE: The hardcoded numbers used here are all primes. Perhaps not a good idea to hard code them; fine for now

            //We choose an upper and lower bound for the algorithm later
            int rngLowerBound = std::hash<std::string> {}(_kr.GetPassword3()) % 313;
            int rngUpperBound = (std::hash<std::string> {}(_kr.GetPassword4()) % 997) + 313;
            int rng;

            //We choose a random starting point in each password
            int encryptIter = rand() % _kr.GetPassword0().size();
            int augmentIter = rand() % _kr.GetPassword1().size();

            //Variables used during encryption/decryption
            std::string temp;
            uint8_t tempchar;

            //Now we do the encryption/decryption
            for (unsigned int cnt = 0; cnt < _data.size(); cnt++)
            {
                temp = reinterpret_cast<char*>(&_data[cnt]);
                tempchar = temp[0];

                if (amEncrypting)
                    tempchar += _kr.GetPassword0()[(cnt + encryptIter) % _kr.GetPassword0().size()];
                else
                    tempchar -= _kr.GetPassword0()[(cnt + encryptIter) % _kr.GetPassword0().size()];

                rng = rand() % 1223;

                if (rng < rngLowerBound)
                {
                    if (amEncrypting)
                        tempchar += _kr.GetPassword1()[(cnt + augmentIter) % _kr.GetPassword1().size()];
                    else
                        tempchar -= _kr.GetPassword1()[(cnt + augmentIter) % _kr.GetPassword1().size()];
                }
                else if (rng > rngUpperBound)
                {
                    if (amEncrypting)
                        tempchar -= _kr.GetPassword1()[(cnt + augmentIter) % _kr.GetPassword1().size()];
                    else
                        tempchar += _kr.GetPassword1()[(cnt + augmentIter) % _kr.GetPassword1().size()];
                }
                else
                {
                    //Do nothing
                }

                //Ensure new value is still within range
                tempchar = (unsigned char)tempchar % 256;

                //Return back to _data
                _data[cnt] = std::bitset<8>(tempchar);
            }
        }
    }

    KeyRing m_kr;
    std::string temp;

public:
    virtual std::string Command(std::string command, std::string options)
    {
        temp = "Received command: ";
        temp += command;
        temp += " with options: ";
        temp += options;

        return temp;
    }

    virtual std::string GetPluginName()
    {
        return "RNCypher";
    }

    virtual std::vector<std::string> RecommendedUses()
    {
        std::vector<std::string> vec;
        vec.push_back("Storage");
        return vec;
    }

    virtual void HandleDataENC(std::vector<std::string> _kr, std::vector< std::bitset<8> >& _data)
    {
        m_kr.SetName(_kr[0]);
        m_kr.SetHash(_kr[1]);
        m_kr.SetEncryptionType(_kr[2]);
        m_kr.SetPassword0(_kr[3]);
        m_kr.SetPassword1(_kr[4]);
        m_kr.SetPassword2(_kr[5]);
        m_kr.SetPassword3(_kr[6]);
        m_kr.SetPassword4(_kr[7]);
        m_kr.SetPassword5(_kr[8]);

        HandleData(m_kr, _data, true);
        return;
    }

    virtual void HandleDataDEC(std::vector<std::string> _kr, std::vector< std::bitset<8> >& _data)
    {
        m_kr.SetName(_kr[0]);
        m_kr.SetHash(_kr[1]);
        m_kr.SetEncryptionType(_kr[2]);
        m_kr.SetPassword0(_kr[3]);
        m_kr.SetPassword1(_kr[4]);
        m_kr.SetPassword2(_kr[5]);
        m_kr.SetPassword3(_kr[6]);
        m_kr.SetPassword4(_kr[7]);
        m_kr.SetPassword5(_kr[8]);

        HandleData(m_kr, _data, false);
        return;
    }

    virtual void HandleByteENC(char& _byte)
    {
        //Not very efficient, but we can reuse our existing code this way
        //Create a temporary vector
        std::vector<std::bitset<8>> vec;

        vec.push_back(std::bitset<8>(_byte));

        //Encrypt that vector
        HandleData(m_kr, vec, false);

        //Change our byte
        temp = reinterpret_cast<char*>(&vec[0]);
        _byte = temp[0];
        return;
    }

    virtual void HandleByteDEC(char& _byte)
    {
        std::vector<std::bitset<8>> vec;
        vec.push_back(std::bitset<8>(_byte));
        HandleData(m_kr, vec, true);

        temp = reinterpret_cast<char*>(&vec[0]);
        _byte = temp[0];
        return;
    }

    virtual void InitializeByteStream(std::vector<std::string> _kr)
    {
        m_kr.SetName(_kr[0]);
        m_kr.SetHash(_kr[1]);
        m_kr.SetEncryptionType(_kr[2]);
        m_kr.SetPassword0(_kr[3]);
        m_kr.SetPassword1(_kr[4]);
        m_kr.SetPassword2(_kr[5]);
        m_kr.SetPassword3(_kr[6]);
        m_kr.SetPassword4(_kr[7]);
        m_kr.SetPassword5(_kr[8]);
    }
};

DEFINE_PLUGIN(RNCypherClass, "RNCypher", "1", "1")
