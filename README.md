# Encoder

Security software suite:

Handles encryption and decryption of files for storage and communication purposes.

## Foundational System

KeyRings store some number of passwords, as well as a (unique) hash and an encryption type.

Encryption Types (for now):

* RN Cypher

## Features

* Easy to set up and use, and very user-friendly (via an implementation of Tiny File Dialogs)
* Highly extensible via user-created plugins (cyphers)
* Heavily documented code, for both plugins and the main application
* A built-in cypher, called the RNCypher which was designed as an upgrade to the German Enigma cypher
* Targets Linux (and possibly OSX) platforms, but a Windows port should not be very difficult

## Project Status

Feature | Status<br />
KeyRing System | COMPLETED<br />
RNCypher System | COMPLETED<br />
RSA Implementation/System | NOT RELEVANT<br />
File Dialog | COMPLETED<br />
Save Settings | COMPLETED<br />
Plugins (Bonus) | COMPLETED<br />

## KeyRing System

The application stores all of your KeyRings in a single encrypted repository. Currently, the capability to encrypt and then decrypt this repository has been demonstrated, and the KeyRings have been successfully (losslessly) extracted. The repository can be encrypted by any function that accepts a `std::vector<std::bitset<8>>` and which is then inversible by some means. For the current implementation, the 'encryption' simply flips every bit.

The system also is able to store meta data (currently it only stores the version ... of something). It stores the meta data somewhere 'randomly' within the file (as in not necessarily at the start or end) making it so that attackers trying to crack the repository will not know what any given bytes in the file do. It also provides a simple way of checking if the file was correctly decrypted, in that if the `$VERSION` tag is never found in the 'decrypted' file, we know it was not properly decrypted.

If you wish to use Encoder, be aware of the cypher used to encrypt the KeyRing Repository, as it is the weakest link in the security chain (since it holds all of the other keys). 

However, from a functional point of view, the program can continue development in that the other systems that require the repository have a sufficient foundation to be built upon.

## RNCypher System

This is an encryption/decryption system of my own creation, intended for storage (as opposed to communication, like RSA) purposes.

Its implementation is as follows: 

It requires two passwords and a seed.

The first password is used as a 'base', to be used like a Ceaser Cypher, except with a string instead of a single character.
The second password is used as a modifier, either adding, subtracting, or not changing the character.
The third password is used as the seed for a random number generator (hence the cypher is called the RNCypher, or Random Number Cypher).

For a message `m`, we have the cypher-text `c` defined as follows:
`c[i] = m[i] + password0[k] + password1[m]*RN` where `RN` is one of `+1, -1, 0`. The distribution of each of these values can be set (perhaps using some of the remaining passwords in a keyring) or the defaults used.

In theory, this method will not have a repeating `password0[k] + password1[m]*RN` component for just about any input data given a reasonably-sized (ie. human-memorable) `password0` and `password1`. Therefore, trying to crack this cypher should be pretty much impossible.

It is also based somewhat on the German Enigma Cypher, except with two advantages/alterations which cover up Enigma's main weaknesses.

* There is no header.
* A byte can be 'encrypted' as itself.

### There is no header.

This is a design goal throughout the entire project: making it so that an attacker that knows what contents a file *should* have does not have enough information to be able to try to brute-force some part of the encryption scheme. Case in point: even though an attacker knows that the file contains a line like `$VERSION = 1.0`, they do not know *where* in the file this is (may or may not be the first few bytes, or the last few bytes, or some arbitrary bytes somewhere in the middle). Therefore, they cannot use this information to try to crack the file's encryption.

### A byte can be 'encrypted' as itself.

Another of Enigma's flaws is that (supposedly) a character cannot be encoded as itself. Therefore, using probability analysis, an attacker could try to 'predict' the cypher from a statistic point of view. Since the repository is essentially plain-text before encryption, this attack vector could theoretically be used. However, the fact that not only can a byte be encrypted as itself, but also that the likelihood of this being the case is considered part of the cypher key, the difficulty of cracking the cypher goes up significantly.


Therefore, even though the entire cypher code can be viewed by anybody, so long as the passwords used to encode it are kept secret, the cypher should be uncrackable given today's current technology.

## RSA System

Named after three MIT graduates, this encryption algorithm is a good partner to my RNCypher in that it is very useful for communication purposes, since it features asymmetrical encryption/decryption. Once a communication channel is established, each user can generate a public-private key pair, and share their public keys. Then, anyone can send them a message (in particular, the other user in the channel), but only they can decode it (using their private key).

RSA takes advantage of several properties of prime numbers, and in particular, large primes and semi-primes. 

It is one of the most secure encryption algorithms; as a matter of fact, this is the algorithm used by major banksand other companies handling sensitive information.

For additional information on RSA, a starting point to consult is available here: https://en.wikipedia.org/wiki/RSA_(cryptosystem)

This system is marked as 'NOT RELEVANT' because there exist far better libraries than this one out there that implement this feature. It is also fairly easy to take one of those and implement it as a plugin for this project.

## File Dialog

An implementation of Tiny File Dialogs so that users can select the files they wish to load and save from a system-native file selection dialog box, instead of using the console. This makes the program more user friendly, and minimizes the chance of an error.

## Save Settings

The program keeps track of several settings for the user, such as what cypher they would like to use for their Key Ring Repository, as well as quality of life features like remembering the user's last load and save locations, as well as offering the user the ability to use the cypher stored by their Key Ring, or to manually override that behaviour and select a different cypher.

## Plugins (Bonus)

This feature was not planned from the beginning, however, it has a natural fit in this project. 

Instead of users relying on a particular implementation of a cypher, they can create their own! So long as they provide functions to encrypt and decrypt a 'std::vector<std::bitset<8>>' and functions to encrypt and decrypt a 'char', they can use any algorithm they wish. 

The possibilities are limitless!

Note: The reason they must provide two seperate encryption and decryption functions is that one pair (the pair handling 'std::vector<std::bitset<8>>'s) is used for general files, and the other pair (handling 'char's) is used as a streaming encryption, which should be capable of handling files of undefined size.

A base (empty) plugin is also provided, along with the RNCypher, to help facilitate the creation of custom encryption types.

Plugins were based off of code by Claus Witt.

