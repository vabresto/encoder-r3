#include <iostream>
#include <dirent.h>
#include <memory>
#include <vector>
#include <map>
#include <fstream>

#include "Common Addons/tinyfiledialogs.h"

#include "PluginManager.h"
#include "KeyRingManager.h"
#include "ActionLoop.h"

//Global Variables
KeyRingManager krm;
bool hardQuit = false;

bool didInit = false;

std::vector<PluginHandler> load_plugins()
{
    std::vector<PluginHandler> plugins;

    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir ("Plugins")) != NULL)
    {
        while ((ent = readdir (dir)) != NULL)
        {
            if(ent->d_name[0] != '.')
                plugins.push_back(PluginHandler("Plugins/" + std::string(ent->d_name)));
        }
        closedir (dir);
    }
    return plugins;
}

bool Init()
{
    if (didInit)
        return false;

    krm.LoadSettingsFile();

    auto plugins = load_plugins();
    std::cout << "Starting ..." << std::endl;
    for (auto ph : plugins)
    {
        auto plugin = ph.load();

        std::cerr << "Loaded plugin: " << ph.get_name();
        std::cerr << "\n[" << ph.get_name() << "]: Version: " << ph.get_version();
        std::cerr << "\n[" << ph.get_name() << "]: Expected Application Version: " << ph.get_expVersion() << std::endl;

        //Because we are using a map, if two plugins have the same name, only the first one will be loaded
        krm.GetPluginsList().insert(std::pair<std::string, std::shared_ptr<Plugin>>(ph.get_name(), plugin));
    }

    if (krm.GetPluginsList().size() == 1)
    {
        std::cout << "[ERROR]: Unable to load any plugins!" << std::endl;
        getchar();
        return 0;
    }
    else
    {
        std::cout<< "Done! Loaded " << krm.GetPluginsList().size()-1 << " plugin(s)."<< std::endl;
        didInit = true;
    }
    return 1;
}

void Run()
{
    selectedValidPluginName = false;

    if (krm.GetKeyRepLocation() == "")
    {
        while (saveFileName == nullptr)
        {
            std::cout << "No default location has been set for your KeyRing repository. \nLet's do this now!" << std::endl;
            getchar();

            saveFileName = tinyfd_saveFileDialog(
                               "Default KeyRing Repository Location",
                               krm.GetDefaultSaveFilePath().c_str(),
                               0,
                               filters,
                               NULL
                           );

            if (saveFileName == nullptr)
            {
                std::cout << "Would you like to abort? [Y]es/[N]o:";
                std::cin >> userInput;
                if (userInput == 'y' || userInput == 'Y')
                    return;
            }

        }

        krm.GetKeyRepLocation() = (std::string)saveFileName;
        //We set it to end, because begin is always 'ERROR' ... or we could ask the user

        std::cout << "How would you like to encrypt your KeyRing repository?" << std::endl;
        while(!selectedValidPluginName)
        {
            std::cout << "Select the encoder you wish to use (or ^Q to quit): " << std::endl;
            std::cout << krm.ListLoadedPlugins() << std::endl;
            std::cin >> pluginName;

            if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                    krm.GetPluginsList().find(pluginName)->second != nullptr)
            {
                selectedValidPluginName = true;
            }
            //Allow the user to quit, if they change their mind
            else if (pluginName == "^Q" || pluginName == "^q")
            {
                return;
            }
        }

        std::cout << "Now we need the 6 passwords with which to lock the KeyRing repository.\nMake sure you DO NOT FORGET these, as you cannot recover them!" << std::endl;

        KeyRing kr;
        kr.SetEncryptionType(pluginName);

        std::string working;
        std::cin >> working;
        kr.SetPassword0(working);
        std::cin >> working;
        kr.SetPassword1(working);
        std::cin >> working;
        kr.SetPassword2(working);
        std::cin >> working;
        kr.SetPassword3(working);
        std::cin >> working;
        kr.SetPassword4(working);
        std::cin >> working;
        kr.SetPassword5(working);
        krm.GetGlobalLock() = kr;


        krm.SetUseKeyRingPlugin(true);
        krm.SetDefaultKeyRepPlugin(pluginName);
        krm.SaveSettingsFile();

        std::cout << "We've set up the basics now.\nEnsure you have AT LEAST TWO KEYRINGS before quitting!" << std::endl;
    }
    else
    {
        std::cout << "Please unlock the KeyRing Repository: " << std::endl;

        while(!selectedValidPluginName)
        {
            std::cout << "Select the decoder you wish to use (or ^Q to quit): " << std::endl;
            std::cout << krm.ListLoadedPlugins() << std::endl;
            std::cin >> pluginName;

            if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                    krm.GetPluginsList().find(pluginName)->second != nullptr)
            {
                selectedValidPluginName = true;
            }
            //Allow the user to quit, if they change their mind
            else if (pluginName == "^Q" || pluginName == "^q")
            {
                return;
            }
        }

        KeyRing kr;
        kr.SetEncryptionType(pluginName);

        std::cout << "Please enter the 6 lock passwords: " << std::endl;

        std::string working;
        std::cin >> working;
        kr.SetPassword0(working);
        std::cin >> working;
        kr.SetPassword1(working);
        std::cin >> working;
        kr.SetPassword2(working);
        std::cin >> working;
        kr.SetPassword3(working);
        std::cin >> working;
        kr.SetPassword4(working);
        std::cin >> working;
        kr.SetPassword5(working);
        krm.GetGlobalLock() = kr;

        krm.LoadKeyRingsFromFile(krm.GetKeyRepLocation(), krm.GetGlobalLock());
    }

    if (!hardQuit)
    {
        //We cheekily run the function as a condition to the while loop
        while (actionLoop())
        {
            //Do nothing
        }
    }
}

int main()
{
    if (Init())
    {
        Run();

        //Probably don't need to do this every time
        krm.SaveSettingsFile();
    }

    return 0;
}


//NOTE: Plugin code based off of code by Claus Witt (http://www.clauswitt.com/creating-a-plugin-system-with-cpp/)
//NOTE: dlfcn is Linux (possibly OSX?) specific. Will need to make modifications for Windows.
