#include "KeyRing.h"
#include <iostream>

KeyRing::KeyRing()
{
}

KeyRing::~KeyRing()
{
}

void KeyRing::SetName(std::string _name)
{
    m_name = _name;
}

std::string KeyRing::GetName()
{
    return m_name;
}

void KeyRing::SetEncryptionType(std::string _type)
{
    m_encryptionType = _type;
}

std::string KeyRing::GetEncryptionType()
{
    return m_encryptionType;
}

void KeyRing::SetHash(std::string _hash)
{
    m_hash = _hash;
}

std::string KeyRing::GetHash()
{
    return m_hash;
}

void KeyRing::SetPassword0(std::string _new)
{
    m_password0 = _new;
}
std::string KeyRing::GetPassword0()
{
    return m_password0;
}

void KeyRing::SetPassword1(std::string _new)
{
    m_password1 = _new;
}
std::string KeyRing::GetPassword1()
{
    return m_password1;
}

void KeyRing::SetPassword2(std::string _new)
{
    m_password2 = _new;
}
std::string KeyRing::GetPassword2()
{
    return m_password2;
}

void KeyRing::SetPassword3(std::string _new)
{
    m_password3 = _new;
}
std::string KeyRing::GetPassword3()
{
    return m_password3;
}

void KeyRing::SetPassword4(std::string _new)
{
    m_password4 = _new;
}
std::string KeyRing::GetPassword4()
{
    return m_password4;
}

void KeyRing::SetPassword5(std::string _new)
{
    m_password5 = _new;
}
std::string KeyRing::GetPassword5()
{
    return m_password5;
}

std::string KeyRing::Serialize()
{
    std::string temp = "";

    temp += m_name + "\n" + m_hash + "\n" ;
    //temp += SerializeEncryptionType();
    temp += m_encryptionType;
    temp += "\n" + m_password0 + "\n" + m_password1 + "\n" + m_password2 + "\n" + m_password3 + "\n" + m_password4 + "\n" + m_password5 + "\n";

    return temp;
}

std::string KeyRing::SerializeEncryptionType()
{
/*
    switch (m_encryptionType)
    {
        case EncryptionType::RNCypher:
        {
            return "RNCypher";
            break;
        }
        case EncryptionType::RSADecrypt:
        {
            return "RSADecrypt";
            break;
        }
        case EncryptionType::RSAEncrypt:
        {
            return "RSAEncrypt";
            break;
        }
        case EncryptionType::ERROR:
        {
            return "ERROR";
            break;
        }
        default:
        {
            std::cout << "Attempting to serialize undefined EncryptionType!" << std::endl;
            return "ERROR";
            break;
        }
    }
    */
    return m_encryptionType;
}

std::vector<std::string> KeyRing::SerializeToList()
{
    std::vector<std::string> vec;

    vec.push_back(m_name);
    vec.push_back(m_hash);
    vec.push_back(m_encryptionType);
    vec.push_back(m_password0);
    vec.push_back(m_password1);
    vec.push_back(m_password2);
    vec.push_back(m_password3);
    vec.push_back(m_password4);
    vec.push_back(m_password5);

    return vec;
}
