#ifndef PLUGINHANDLER_H_INCLUDED
#define PLUGINHANDLER_H_INCLUDED


#if defined(_WIN32)
#include "dlfcn.h"
#else
#include <dlfcn.h>
#endif

#include <memory>

#include "Plugin.h"


class PluginHandler
{
    std::shared_ptr<Plugin> (*_load)();
    void* handle;
    char* (*_get_name)();
    char* (*_get_version)();
    char* (*_get_expVersion)();

    std::shared_ptr<Plugin> instance;
public:

    PluginHandler(std::string name)
    {
        handle = dlopen(name.c_str(), RTLD_LAZY);
        _load = (std::shared_ptr<Plugin> (*)())dlsym(handle, "load");
        _get_name = (char* (*)())dlsym(handle, "name");
        _get_version = (char* (*)())dlsym(handle, "version");
        _get_expVersion = (char* (*)())dlsym(handle, "expVersion");
    }

    std::string get_name()
    {
        return std::string(_get_name());
    }

    std::string get_version()
    {
        return std::string(_get_version());
    }

    std::string get_expVersion()
    {
        return std::string(_get_expVersion());
    }

    std::shared_ptr<Plugin> load()
    {
        if(!instance)
            instance = _load();
        return instance;
    }

};

#endif // PLUGINHANDLER_H_INCLUDED
