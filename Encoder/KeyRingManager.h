#pragma once

#include <string>
#include <vector>
#include <map>
#include <bitset>
#include <memory>

#include "Plugin.h"
#include "KeyRing.h"

class KeyRing;

//This will be a singleton class, responsible for handling the keyrings
class KeyRingManager
{
private:

    //List of all plugins/cyphers that were loaded
    std::map<std::string, std::shared_ptr<Plugin> > s_pluginsList;
    //List of all KeyRings that were loaded
    std::map<std::string, KeyRing> m_keyRingsList;

    //Version according to the settings file ... possibly not the best place to put it
    unsigned int m_versionMaj, m_versionMin;

    //Flag for whether or not to ask user for what cypher to use with a keyring (allows overriding)
    bool m_useKeyRingPlugin;
    //Plugin to be used to encode the KeyRep file
    //TODO: Implement KeyRep file encoding based on cyphers
    std::string m_defaultKeyRepPlugin;

    //Default file path to open when loading/saving files
    std::string m_defaultSaveFilePath;
    std::string m_defaultLoadFilePath;

    std::string m_keyRep = "";

    std::string m_settingsFilePath = "settings.txt";

    KeyRing m_globalLock;

    bool StringExistsInVector(std::string _huh, std::vector<std::string> _vec);

public:

    //Constructor and destructor
    KeyRingManager();
    ~KeyRingManager();

    //We want to return reference so that we can update the functions later
    std::map<std::string, std::shared_ptr<Plugin>>& GetPluginsList();
    std::map<std::string, KeyRing>& GetKeyRingsList();

    //Return a string with the version according to the KeyRingManager
    std::string GetVersion();

    //Saving the entire keyrings list to file, and loading back
    void LoadKeyRingsFromFile(std::string _file, KeyRing _kr);
    void SaveKeyRingsToFile(std::string _file, KeyRing _kr, std::vector<std::string> _whitelist = std::vector<std::string>());

    //Load and save the settings file
    void LoadSettingsFile();
    void SaveSettingsFile();

    //Create, destroy or modify a keyring
    void CreateNewKeyRing();
    void DeleteKeyRing(std::string _hash);
    void ModifyKeyRing(std::string _hash);

    //This will only be called on the container - we may change this later; but it must work independent of the Key Rings
    void EncryptKeyRings(std::vector<std::bitset<8>>& _data, KeyRing _kr);
    void DecryptKeyRings(std::vector<std::bitset<8>>& _data, KeyRing _kr);

    //Return a formatted string with all of the Key Rings
    std::string ListKeyRings();

    //Generate a not-very-random hash - probably better to just use a built-in hash function
    std::string GenerateHash(unsigned int _size);

    //Fetch a keyring's hash by looking up its name
    std::string FetchHashByName(std::string _name);

    //Return a formatted string with all of the loaded Plugins
    std::string ListLoadedPlugins();

    //Return and set the 'm_useKeyRingPlugin' flag
    bool UseKeyRingPlugin();
    void SetUseKeyRingPlugin(bool _new);

    std::string GetDefaultKeyRepPlugin();
    void SetDefaultKeyRepPlugin(std::string _new);

    //Getters and Setters for default Load and Save paths
    std::string GetDefaultSaveFilePath();
    void SetDefaultSaveFilePath(std::string _newPath);

    std::string GetDefaultLoadFilePath();
    void SetDefaultLoadFilePath(std::string _newPath);

    std::string& GetKeyRepLocation();

    KeyRing& GetGlobalLock();
};
