#include <iostream>
#include <dirent.h>
#include <memory>
#include <vector>
#include <map>
#include <fstream>

#include "Common Addons/tinyfiledialogs.h"

#include "KeyRing.h"
#include "ActionLoop.h"

char userInput;
std::string inp;
std::string krName;
const char * openFileName = nullptr;
const char * saveFileName = nullptr;
const char* filters[1] = {"*.*"};
bool selectedValidPluginName = false;
bool selectedValidName = false;
std::string pluginName;
std::string working;
KeyRing kr;

//Max file size (in bytes) before switching to stream encryption instead
unsigned int MAX_FILE_SIZE = 1000000;


void ManageKeyRings()
{
    while (true)
    {
//List key rings so user knows what they have
        std::cout << krm.ListKeyRings() << std::endl;

        std::cout << "What action would you like to perform?" << std::endl;
        std::cout << "-> [C]reate new KeyRing" << "\n-> [M]odify a KeyRing" << "\n-> [D]elete a KeyRing"  << "\n-> [I]mport KeyRings"  << "\n-> [E]xport KeyRings"  << "\n-> [R]eturn" << std::endl;

        std::cin >> userInput;
        switch (userInput)
        {
        case 'c':
        case 'C':
        {
            //Create the new key ring
            krm.CreateNewKeyRing();
            //Make sure to save the new key ring now
            krm.SaveKeyRingsToFile(krm.GetKeyRepLocation(), krm.GetGlobalLock());
            break;
        }
        case 'm':
        case 'M':
        {
            //Select the key ring to be modified
            std::cout << "Please enter the KeyRing's name: ";
            std::cin >> inp;

            //Modify and save right away
            krm.ModifyKeyRing(krm.FetchHashByName(inp));
            krm.SaveKeyRingsToFile(krm.GetKeyRepLocation(), krm.GetGlobalLock());
            break;
        }
        case 'd':
        case 'D':
        {
            //Select the key ring to be deleted
            std::cout << "Please enter the KeyRing's name: ";
            std::cin >> inp;

            //Delete and save right away
            krm.DeleteKeyRing(krm.FetchHashByName(inp));
            krm.SaveKeyRingsToFile(krm.GetKeyRepLocation(), krm.GetGlobalLock());
            break;
        }
        case 'i':
        case 'I':
        {
            //Remember how many keys we had before trying to import
            unsigned int numKeys = krm.GetKeyRingsList().size();
            openFileName = nullptr;
            //Reset valid plugin flag
            selectedValidPluginName = false;

            //Ensure the user selects a valid plugin (encryptor)
            while(!selectedValidPluginName)
            {
                std::cout << "Select the decoder you wish to use (or ^Q to Quit): " << std::endl;
                std::cout << krm.ListLoadedPlugins() << std::endl;
                std::cin >> pluginName;

                if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                        krm.GetPluginsList().find(pluginName)->second != nullptr)
                {
                    selectedValidPluginName = true;
                }
                //Allow the user to quit, if they change their mind
                else if (pluginName == "^Q" || pluginName == "^q")
                {
                    ManageKeyRings();
                }
            }

            std::cout << "Please enter the decryption passwords: " << std::endl;
            std::cin >> working;
            kr.SetPassword0(working);
            std::cin >> working;
            kr.SetPassword1(working);
            std::cin >> working;
            kr.SetPassword2(working);
            std::cin >> working;
            kr.SetPassword3(working);
            std::cin >> working;
            kr.SetPassword4(working);
            std::cin >> working;
            kr.SetPassword5(working);

            //Ensure the user picks a file
            while (openFileName == nullptr)
            {
                openFileName = tinyfd_openFileDialog(
                                   "Select File to Import",
                                   krm.GetDefaultLoadFilePath().c_str(),
                                   0,
                                   filters,
                                   NULL,
                                   0
                               );

                //Allow the user to quit if they change their mind
                if (openFileName == nullptr)
                {
                    std::cout << "Would you like to abort? [Y]es/[N]o:";
                    std::cin >> userInput;
                    if (userInput == 'y' || userInput == 'Y')
                        ManageKeyRings();
                }
            }

            //Remember the last place the user loaded
            krm.SetDefaultLoadFilePath(openFileName);

            //We use LoadKeyRingsFromFile to do the import
            krm.LoadKeyRingsFromFile((std::string)openFileName, kr);

            //We see if we added any new keys to tell if the import was successful
            if (krm.GetKeyRingsList().size() > numKeys)
            {
                std::cout << "Successfully loaded " << krm.GetKeyRingsList().size() - numKeys << " new keys!" << std::endl;
            }
            else
            {
                std::cout << "[WARNING]: No new keys were loaded!" << std::endl;
            }

            //Save our changes right away
            krm.SaveKeyRingsToFile(krm.GetKeyRepLocation(), krm.GetGlobalLock());
            break;
        }
        case 'e':
        case 'E':
        {
            //List of keys to be exported
            std::vector<std::string> keysToExp;
            saveFileName = nullptr;

            std::cout << "Please select the KeyRings you would like to export: " << std::endl;
            std::cout << krm.ListKeyRings();
            std::cout << "Enter ^S to save (and continue), ^Q to quit and ^L to list the KeyRings again." << std::endl;

            //Allow the user to keep adding keys until they are satisfied
            while (true)
            {
                std::cin>>inp;
                if (inp == "^s" || inp =="^S")
                {
                    break;
                }
                else if (inp == "^q" || inp =="^Q")
                {
                    ManageKeyRings();
                }
                else if (inp == "^l" || inp =="^L")
                {
                    std::cout << krm.ListKeyRings();
                }
                else
                {
                    if (krm.GetKeyRingsList().find(krm.FetchHashByName(inp)) == krm.GetKeyRingsList().end())
                    {
                        std::cout << "Invalid selection; KeyRing does not exist!" << std::endl;
                    }
                    else
                    {
                        keysToExp.push_back(inp);
                    }
                }
            }

            //Show user what keys they selected, and ask for confirmation
            std::cout << "Exporting the following KeyRings: " << std::endl;

            for (unsigned int cnt = 0; cnt < keysToExp.size(); cnt++)
            {
                std::cout << keysToExp[cnt] << std::endl;
            }

            std::cout << "Would you like to export these KeyRings? [Y]es/[N]o:";
            std::cin >> userInput;
            if (userInput == 'Y' || userInput == 'y')
            {
                //Reset flag on valid plugin
                selectedValidPluginName = false;

                //Ensure user picks a valid plugin (encryptor)
                while(!selectedValidPluginName)
                {
                    std::cout << "Select the encoder you wish to use (or ^Q to Quit): " << std::endl;
                    std::cout << krm.ListLoadedPlugins() << std::endl;
                    std::cin >> pluginName;

                    if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                            krm.GetPluginsList().find(pluginName)->second != nullptr)
                    {
                        selectedValidPluginName = true;
                    }
                    //Allow the user to quit, if they change their mind
                    else if (pluginName == "^Q" || pluginName == "^q")
                    {
                        ManageKeyRings();
                    }
                }

                std::cout << "Please enter the encryption passwords: " << std::endl;
                std::cin >> working;
                kr.SetPassword0(working);
                std::cin >> working;
                kr.SetPassword1(working);
                std::cin >> working;
                kr.SetPassword2(working);
                std::cin >> working;
                kr.SetPassword3(working);
                std::cin >> working;
                kr.SetPassword4(working);
                std::cin >> working;
                kr.SetPassword5(working);

                //Ensure user picks a valid file
                while (saveFileName == nullptr)
                {
                    std::cout << "\nPlease select the save location:";
                    saveFileName = tinyfd_saveFileDialog(
                                       "Select Output File",
                                       krm.GetDefaultSaveFilePath().c_str(),
                                       0,
                                       filters,
                                       NULL
                                   );

                    if (saveFileName == nullptr)
                    {
                        std::cout << "Would you like to abort? [Y]es/[N]o:";
                        std::cin >> userInput;
                        if (userInput == 'y' || userInput == 'Y')
                        {
                            ManageKeyRings();
                        }
                    }
                }

                //Update default save path
                krm.SetDefaultSaveFilePath(saveFileName);
                //We export by using SaveKeyRingsToFile with a whitelist parameter
                krm.SaveKeyRingsToFile((std::string)saveFileName, kr, keysToExp);
            }
            else
                break;
        }
        case 'r':
        case 'R':
            return;
        }
    }
}

void ManagePlugins()
{
    while (true)
    {
        std::cout << "What action would you like to perform?" << std::endl;
        std::cout << "-> [L]ist loaded Plugins" << "\n-> [R]eturn" << std::endl;

        std::cin >> userInput;

        switch(userInput)
        {
        case 'l':
        case 'L':
        {
            std::cout << krm.ListLoadedPlugins() << std::endl;
            break;
        }

        case 'r':
        case 'R':
        {
            return;
        }
        }
    }
}

void ManageSettings()
{
    while (true)
    {
        std::cout << "What action would you like to perform?" << std::endl;
        std::cout << "-> [P]rint version" << "\n-> Set [D]efault KeyRep Encoder" << "\n-> [T]oggle Use Recommended Plugin" << "\n-> Clear [S]ave File Path" << "\n-> Clear [L]oad File Path" << "\n-> [R]eturn" << std::endl;

        std::cin >> userInput;

        switch(userInput)
        {
        case 'p':
        case 'P':
        {
            std::cout << "Version: " << krm.GetVersion() << std::endl;
            break;
        }
        case 'd':
        case 'D':
        {
            selectedValidPluginName = false;
            while(!selectedValidPluginName)
            {
                std::cout << "Select the encoder you wish to use (or ^Q to quit): " << std::endl;
                std::cout << krm.ListLoadedPlugins() << std::endl;
                std::cin >> pluginName;

                if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                        krm.GetPluginsList().find(pluginName)->second != nullptr)
                {
                    selectedValidPluginName = true;
                }
                //Allow the user to quit, if they change their mind
                else if (pluginName == "^Q" || pluginName == "^q")
                {
                    break;
                }
                else
                {
                    std::cout << "Invalid plugin!" << std::endl;
                }
            }
            krm.SaveSettingsFile();
            std::cout << "Set KeyRep encoder to '" << pluginName << "'!\n";

            break;
        }
        case 't':
        case 'T':
        {
            krm.SetUseKeyRingPlugin(!krm.UseKeyRingPlugin());
            krm.SaveSettingsFile();
            std::cout << "Use key ring plugin set to " << krm.UseKeyRingPlugin() << std::endl;
            break;
        }
        case 's':
        case 'S':
        {
            krm.SetDefaultSaveFilePath("");
            std::cout << "Save file path reset!" << std::endl;
            break;
        }
        case 'l':
        case 'L':
        {
            krm.SetDefaultLoadFilePath("");
            std::cout << "Load file path reset!" << std::endl;
            break;
        }
        case 'r':
        case 'R':
        {
            return;
        }
        }
    }
}

void EncryptFile()
{
    selectedValidName = false;
    selectedValidPluginName = false;
    openFileName = nullptr;
    saveFileName = nullptr;

    while (openFileName == nullptr)
    {
        std::cout << "Please select the file you wish to encrypt: ";

        openFileName = tinyfd_openFileDialog(
                           "Select File to Encrypt",
                           krm.GetDefaultLoadFilePath().c_str(),
                           0,
                           filters,
                           NULL,
                           0
                       );

        if (openFileName == nullptr)
        {
            std::cout << "Would you like to abort? [Y]es/[N]o:";
            std::cin >> userInput;
            if (userInput == 'y' || userInput == 'Y')
                return;
        }
    }
    //Remember the last place the user loaded
    krm.SetDefaultLoadFilePath(openFileName);

    while(!selectedValidName)
    {
        std::cout << "Select the KeyRing you wish to use: " << std::endl;
        std::cout << krm.ListKeyRings() << std::endl;
        std::cin >> krName;

        if (krm.GetKeyRingsList().find(krm.FetchHashByName(krName)) != krm.GetKeyRingsList().end())
        {
            selectedValidName = true;
        }
    }

    if (!krm.UseKeyRingPlugin())
    {
        while(!selectedValidPluginName)
        {
            std::cout << "Select the encoder you wish to use (or ^Q to quit): " << std::endl;
            std::cout << krm.ListLoadedPlugins() << std::endl;
            std::cin >> pluginName;

            if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                    krm.GetPluginsList().find(pluginName)->second != nullptr)
            {
                selectedValidPluginName = true;
            }
            //Allow the user to quit, if they change their mind
            else if (pluginName == "^Q" || pluginName == "^q")
            {
                return;
            }
        }
    }
    else
    {
        pluginName = krm.GetKeyRingsList().find(krm.FetchHashByName(krName))->second.GetEncryptionType();
    }

    while (saveFileName == nullptr)
    {
        std::cout << "Select the location to output the encrypted file:";
        saveFileName = tinyfd_saveFileDialog(
                           "Output Encrypted File",
                           krm.GetDefaultSaveFilePath().c_str(),
                           0,
                           filters,
                           NULL
                       );
        if (saveFileName == nullptr)
        {
            std::cout << "Would you like to abort? [Y]es/[N]o:";
            std::cin >> userInput;
            if (userInput == 'y' || userInput == 'Y')
                return;
        }
    }


    //Remember the last place the user saved
    krm.SetDefaultSaveFilePath(saveFileName);

    std::ifstream file (openFileName, std::fstream::binary);
    std::ofstream saveFile(saveFileName, std::fstream::binary);
    if (file && saveFile)
    {
        //Setup
        char byte;
        file.seekg(0, file.end);
        unsigned int length = file.tellg();
        file.seekg(0, file.beg);

        //If the file is too big, we will encrypt it via a stream instead
        if (length > MAX_FILE_SIZE)
        {
            //We initialize the stream encoder once, by passing the keyring
            krm.GetPluginsList().find(pluginName)->second->InitializeByteStream(krm.GetKeyRingsList().find(krm.FetchHashByName(krName))->second.SerializeToList());
            while (true)
            {
                file.read(&byte,1);
                krm.GetPluginsList().find(pluginName)->second->HandleByteENC(byte);
                saveFile.write(reinterpret_cast<char*>(byte), 1);

                if (!file)
                    break;
            }
            file.close();
            saveFile.close();
        }
        else
        {
            std::vector<std::bitset<8>> buffer;
            //Read file as bytes
            for (unsigned int cnt = 0; cnt < length; cnt++)
            {
                file.read(&byte, 1);
                buffer.push_back(std::bitset<8>(byte));
                file.seekg(cnt+1);
            }

            //Close the file; we don't want to modify it
            file.close();

            //Encrypt the data
            krm.GetPluginsList().find(pluginName)->second->HandleDataENC(krm.GetKeyRingsList().find(krm.FetchHashByName(krName))->second.SerializeToList(), buffer);

            //Save the encrypted data
            //Now, we start writing
            for (unsigned int cnt = 0; cnt < buffer.size(); cnt++)
            {
                //Is this necessary?
                saveFile.seekp(cnt);
                saveFile.write(reinterpret_cast<char*>(&buffer[cnt]),1);
            }
            saveFile.close();
        }
        std::cout << "File encrypted!" << std::endl;
    }
    else
    {
        std::cout << "[ERROR]: Unable to load file " << openFileName << "!\n";
    }
}

void DecryptFile()
{
    selectedValidName = false;
    selectedValidPluginName = false;
    openFileName = nullptr;
    saveFileName = nullptr;

    while (openFileName == nullptr)
    {
        std::cout << "Please select the file you wish to decrypt: ";
        openFileName = tinyfd_openFileDialog(
                           "Select File to DECRYPT",
                           krm.GetDefaultLoadFilePath().c_str(),
                           0,
                           filters,
                           NULL,
                           0
                       );
        if (openFileName == nullptr)
        {
            std::cout << "Would you like to abort? [Y]es/[N]o:";
            std::cin >> userInput;
            if (userInput == 'y' || userInput == 'Y')
                return;
        }
    }
    //Remember the last place the user loaded
    krm.SetDefaultLoadFilePath(openFileName);

    while(!selectedValidName)
    {
        std::cout << "Select the KeyRing you wish to use: " << std::endl;
        std::cout << krm.ListKeyRings() << std::endl;
        std::cin >> krName;

        if (krm.GetKeyRingsList().find(krm.FetchHashByName(krName)) != krm.GetKeyRingsList().end())
        {
            selectedValidName = true;
        }
    }

    if (!krm.UseKeyRingPlugin())
    {
        while(!selectedValidPluginName)
        {
            std::cout << "Select the decoder you wish to use (or ^Q to quit): " << std::endl;
            std::cout << krm.ListLoadedPlugins() << std::endl;
            std::cin >> pluginName;

            if (krm.GetPluginsList().find(pluginName) != krm.GetPluginsList().end() &&
                    krm.GetPluginsList().find(pluginName)->second != nullptr)
            {
                selectedValidPluginName = true;
            }
            //Allow the user to quit, if they change their mind
            else if (pluginName == "^Q" || pluginName == "^q")
            {
                return;
            }
        }
    }
    else
    {
        pluginName = krm.GetKeyRingsList().find(krm.FetchHashByName(krName))->second.GetEncryptionType();
    }

    while (saveFileName == nullptr)
    {
        std::cout << "Select the location to output the decrypted file:";
        saveFileName = tinyfd_saveFileDialog(
                           "Output Decrypted File",
                           krm.GetDefaultSaveFilePath().c_str(),
                           0,
                           filters,
                           NULL
                       );
        if (saveFileName == nullptr)
        {
            std::cout << "Would you like to abort? [Y]es/[N]o:";
            std::cin >> userInput;
            if (userInput == 'y' || userInput == 'Y')
                return;
        }
    }


    //Remember the last place the user saved
    krm.SetDefaultSaveFilePath(saveFileName);

    std::ifstream file (openFileName, std::fstream::binary);
    std::ofstream saveFile(saveFileName, std::fstream::binary);
    if (file && saveFile)
    {
        //Setup
        char byte;
        file.seekg(0, file.end);
        unsigned int length = file.tellg();
        file.seekg(0, file.beg);

        if (length > MAX_FILE_SIZE)
        {
            //We initialize the stream decoder once, by passing the keyring
            krm.GetPluginsList().find(pluginName)->second->InitializeByteStream(krm.GetKeyRingsList().find(krm.FetchHashByName(krName))->second.SerializeToList());
            while (true)
            {
                file.read(&byte,1);
                krm.GetPluginsList().find(pluginName)->second->HandleByteDEC(byte);
                saveFile.write(reinterpret_cast<char*>(byte), 1);

                if (!file)
                    break;
            }
            file.close();
            saveFile.close();
        }
        else
        {
            std::vector<std::bitset<8>> buffer;
            //Read file as bytes
            for (unsigned int cnt = 0; cnt < length; cnt++)
            {
                file.read(&byte, 1);
                buffer.push_back(std::bitset<8>(byte));
                file.seekg(cnt+1);
            }

            //Close the file; we don't want to modify it
            file.close();

            //Encrypt the data
            krm.GetPluginsList().find(pluginName)->second->HandleDataDEC(krm.GetKeyRingsList().find(krm.FetchHashByName(krName))->second.SerializeToList(), buffer);

            //Now, we start writing
            for (unsigned int cnt = 0; cnt < buffer.size(); cnt++)
            {
                saveFile.seekp(cnt);
                saveFile.write(reinterpret_cast<char*>(&buffer[cnt]),1);
            }

            saveFile.close();
        }
        std::cout << "File Decrypted!" << std::endl;
    }
    else
    {
        std::cout << "[ERROR]: Unable to load file " << openFileName << "!\n";
    }
}

bool actionLoop()
{
    std::cout << "What action would you like to perform?" << std::endl;
    std::cout << "-> Manage [K]eyRings" << "\n-> Manage [P]lugins" << "\n-> Manage [S]ettings" << "\n-> [E]ncrypt a File" << "\n-> [D]ecrypt a File" << "\n-> [Q]uit" << std::endl;

    std::cin >> userInput;

    switch (userInput)
    {
    case 'k':
    case 'K':
    {
        ManageKeyRings();
        break;
    }
    case 'p':
    case 'P':
    {
        ManagePlugins();
        break;
    }
    case 's':
    case 'S':
    {
        ManageSettings();
        break;
    }
    case 'e':
    case 'E':
    {
        EncryptFile();
        break;
    }
    case 'd':
    case 'D':
    {
        DecryptFile();
        break;
    }
    case 'q':
    case 'Q':
    {
        return false;
    }
    }
    return true;
}
