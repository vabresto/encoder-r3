#ifndef PLUGIN_H_INCLUDED
#define PLUGIN_H_INCLUDED

#include <string>
#include <bitset>
#include <vector>
#include "KeyRing.h"

class Plugin {
  public:

    Plugin() {};
    virtual ~Plugin() {};

    /*
    \brief Generic command function. Currently not really used for anything right now.
    \param Two strings; command and options
    \return A string
    */
    virtual std::string Command(std::string command, std::string options) {return "";}

    /*
    \brief Returns the plugin's name to be used in code
    NOTE: The plugin's name should/will EXACTLY match a KeyRing's "encryptionType"
    \param N/A
    \return The plugin's name
    */
    virtual std::string GetPluginName()
    {
        return "REPLACE-ME!";
    }

    /*
    \brief Will do the actual encoding of the data
    \param A vector of bits (the data to be handled)
    \return N/A
    */
    virtual void HandleDataENC(std::vector<std::string> _kr, std::vector< std::bitset<8> >& _data)
    {
        //Base won't do anything to the data
        return;
    }

    /*
    \brief Will do the actual decoding of the data
    \param A vector of bits (the data to be handled)
    \return N/A
    */
    virtual void HandleDataDEC(std::vector<std::string> _kr, std::vector< std::bitset<8> >& _data)
    {
        //Base won't do anything to the data
        return;
    }

    /*
    \brief Will do the actual encoding of the data
    \param A singe byte to be encoded
    \return N/A
    */
    virtual void HandleByteENC(char& _byte)
    {
        //Base won't do anything to the data
        return;
    }

    /*
    \brief Will do the actual decoding of the data
    \param A singe byte to be decoded
    \return N/A
    */
    virtual void HandleByteDEC(char& _byte)
    {
        //Base won't do anything to the data
        return;
    }

    /*
    \brief Prepare to encode/decode the data
    \param A serialized key ring
    \return N/A
    */
    virtual void InitializeByteStream(std::vector<std::string> _kr)
    {
        return;
    }

    /*
    \brief Returns the recommended uses for the plugin; should be one of:
    -> KeyRep           (meaning it should be used to encode/decode the user's keyrep)
    -> Storage           (meaning it should be used to encode/decode generic files for storage)
    -> CommsAll       (meaning it should be used for both encryption and decryption of messages)
    -> CommsEnc     (meaning it should ONLY be used for the ENCRYPTION of messages)
    -> CommsDec     (meaning it should ONLY be used for the DECRYPTION of messages)
    \param N/A
    \return The list of recommended uses.
    */
    virtual std::vector<std::string> RecommendedUses()
    {
        std::vector<std::string> vec;
        return vec;
    }
};


#define DEFINE_PLUGIN(classType, pluginName, pluginVersion, expectedVersion)     \
                                                                               \
    std::shared_ptr<Plugin> load()                                                      \
    {                                                                                                   \
      return std::make_shared<classType>();                                     \
    }                                                                                                   \
                                                                                                        \
    const char* name()                                                                       \
    {                                                                                                   \
      return pluginName;                                                                    \
    }                                                                                                   \
                                                                                                        \
    const char* version()                                                                     \
    {                                                                                                   \
      return pluginVersion;                                                                 \
    }                                                                                                   \
    const char* expVersion()                                                                     \
    {                                                                                                   \
      return expectedVersion;                                                                 \
    }                                                                                                   \
  
 

#endif // PLUGIN_H_INCLUDED
