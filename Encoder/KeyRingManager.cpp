#include "KeyRingManager.h"
#include "KeyRing.h"
#include "ActionLoop.h"

#include <string>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <vector>
#include <iterator>
#include <map>
#include <bitset>
#include <random>
#include <time.h>
#include <algorithm>

KeyRingManager::KeyRingManager()
{
    m_versionMaj = 1;
    m_versionMin = 0;

    s_pluginsList.insert(std::pair<std::string, std::shared_ptr<Plugin> >("ERROR", nullptr));
}

KeyRingManager::~KeyRingManager()
{
}

std::map<std::string, std::shared_ptr<Plugin>>& KeyRingManager::GetPluginsList()
{
    return s_pluginsList;
}

std::map<std::string, KeyRing>& KeyRingManager::GetKeyRingsList()
{
    return m_keyRingsList;
}

std::string KeyRingManager::GetVersion()
{
    std::string ret = std::to_string(m_versionMaj);
    ret += "." + std::to_string(m_versionMin);
    return ret;
}

void KeyRingManager::LoadKeyRingsFromFile(std::string _file, KeyRing _kr)
{
    std::ifstream input(_file.c_str(), std::fstream::binary);
    std::vector<uint8_t> buffer;

    std::vector<std::bitset<8>> bitbuffer;

    std::vector<std::string> stringbuffer;
    std::vector<KeyRing> keyRingRep;

    bool valid = false;

    if (input)
    {
        //Setup
        char byte;
        input.seekg(0, input.end);
        unsigned int length = input.tellg();
        input.seekg(0, input.beg);


        //Read file as bytes
        for (unsigned int cnt = 0; cnt < length; cnt++)
        {
            input.read(&byte, 1);
            buffer.push_back((uint8_t)byte);
            input.seekg(cnt+1);
        }

        for (unsigned int cnt = 0; cnt < buffer.size(); cnt++)
        {
            bitbuffer.push_back(std::bitset<8>(buffer[cnt]));
        }

        DecryptKeyRings(bitbuffer, _kr);

        //May possibly have to reconsider this entire method....
        std::string tempStr = "";
        for (unsigned int cnt = 0; cnt < bitbuffer.size(); cnt++)
        {
            if (bitbuffer[cnt] == '\n')
            {
                stringbuffer.push_back(tempStr);
                tempStr = "";
            }
            else
            {
                tempStr += reinterpret_cast<char*>(&bitbuffer[cnt]);
            }
        }

        // Now we need to start parsing the data
        unsigned int numData = 0;
        KeyRing kr;
        keyRingRep.push_back(kr);

        for (unsigned int cnt = 0; cnt < stringbuffer.size(); cnt++)
        {
            //We know this is a meta tag item
            if (stringbuffer[cnt].find("$VERSION") != std::string::npos)
            {
                //Fetch the version
                std::string temp = stringbuffer[cnt].substr(stringbuffer[cnt].find('='));
                std::string tempMaj = temp.substr(1, temp.find('.')-1);
                std::string tempMin = temp.substr(temp.find('.')+1);

                //Convert and save to the manager
                m_versionMaj = std::max(m_versionMaj, (unsigned int)std::stoi(tempMaj));
                m_versionMin = std::max(m_versionMin, (unsigned int)std::stoi(tempMin));

                valid = true;
            }
            else if (stringbuffer[cnt].find("$META") != std::string::npos)
            {
                //TODO: Handle META info (if needed); Make sure to add to SAVE as well
            }
            else if (stringbuffer[cnt][0] == '-')
            {
                //Do nothing
            }
            else
            {
                //This data is part of a keyring
                switch(numData)
                {
                case 0:
                {
                    //Name
                    keyRingRep[keyRingRep.size() - 1].SetName(stringbuffer[cnt]);
                    break;
                }
                case 1:
                {
                    //Hash
                    keyRingRep[keyRingRep.size() - 1].SetHash(stringbuffer[cnt]);
                    break;
                }
                case 2:
                {
                    //EncryptionType
                    if (s_pluginsList.find(stringbuffer[cnt]) == s_pluginsList.end())
                    {
                        //Invalid cypher type
                        std::cout << "Invalid cypher type! No cypher type was assigned to KeyRing " << keyRingRep[keyRingRep.size() - 1].GetName() << std::endl;
                        keyRingRep[keyRingRep.size() - 1].SetEncryptionType("ERROR");
                    }
                    else
                    {
                        keyRingRep[keyRingRep.size() - 1].SetEncryptionType(stringbuffer[cnt]);
                    }
                    break;
                }
                case 3:
                {
                    //P0
                    keyRingRep[keyRingRep.size() - 1].SetPassword0(stringbuffer[cnt]);
                    break;
                }
                case 4:
                {
                    //P1
                    keyRingRep[keyRingRep.size() - 1].SetPassword1(stringbuffer[cnt]);
                    break;
                }
                case 5:
                {
                    //P2
                    keyRingRep[keyRingRep.size() - 1].SetPassword2(stringbuffer[cnt]);
                    break;
                }
                case 6:
                {
                    //P3
                    keyRingRep[keyRingRep.size() - 1].SetPassword3(stringbuffer[cnt]);
                    break;
                }
                case 7:
                {
                    //P4
                    keyRingRep[keyRingRep.size() - 1].SetPassword4(stringbuffer[cnt]);
                    break;
                }
                case 8:
                {
                    //P5
                    keyRingRep[keyRingRep.size() - 1].SetPassword5(stringbuffer[cnt]);
                    break;
                }
                }
                numData++;

                if (numData >8)
                {
                    numData = 0;

                    KeyRing kr;
                    keyRingRep.push_back(kr);

                    //We want to increment cnt an extra time to skip the line with "-"
                    cnt ++;
                }
            }
        }

        input.close();

        //Sub-optimal, but we will now process this list into the final map to be used during runtime
        for (unsigned int cnt = 0; cnt < keyRingRep.size(); cnt++)
        {
            //Store by hash
            m_keyRingsList.insert(std::pair<std::string, KeyRing>(keyRingRep[cnt].GetHash(), keyRingRep[cnt]));
        }

        if (!valid)
        {
            //Loaded file did not have a version number; hence, error
            std::cout << "[ERROR]: Loaded KeyRing Repository file may have been corrupted/not decoded correctly!" << std::endl;
            //We want to go back to 'start' so that the user doesn't accidentally corrupt their keyrings
            Run();
            hardQuit = true;
        }
        else
        {
            //Finished
            std::cout << "Done loading keyrings!" <<std::endl;
            hardQuit = false;
        }
    }
    else
    {
        std::cout << "[ERROR]:\tError loading file " << _file << std::endl;
    }
}

bool KeyRingManager::StringExistsInVector(std::string _huh, std::vector<std::string> _vec)
{
    for (unsigned int cnt = 0; cnt < _vec.size(); cnt++)
    {
        if (_vec[cnt] == _huh)
            return true;
    }
    return false;
}

void KeyRingManager::SaveKeyRingsToFile(std::string _file, KeyRing _kr, std::vector<std::string> _whitelist)
{
    std::vector<std::bitset<8>> buffer;
    std::string output = "";
    std::ofstream saveFile(_file.c_str(), std::fstream::binary);

    //TODO: Ensure this RNG is seeded before calling it .. though as long as it doesn't always put it first, I don't mind
    unsigned int headerpos = rand() % (int)std::max((long long)1, (long long) std::max(m_keyRingsList.size(), _whitelist.size())-1);
    unsigned int iter = 0;


    //We want to save and encrypt
    for (auto& itr : m_keyRingsList)
    {
        if (itr.second.GetHash() != "" && itr.second.GetName() != "" && (_whitelist.size() == 0 || StringExistsInVector(itr.second.GetName(), _whitelist)))
        {
            output += itr.second.Serialize();
            output += "-\n";
        }

        if (headerpos == iter)
        {
            output += "$VERSION=";
            output += std::to_string(m_versionMaj);
            output +=  ".";
            output += std::to_string(m_versionMin) + "\n";
        }
        iter ++;
    }

    //We want to cut short the string so the last character is unpredictable
    //Therefore, we want to cut the '-' and the "\n" so that the last character in the file
    // is the last character of P5
    output.resize(output.size() - 3);

    if (iter < headerpos)
    {
        output += "\n$VERSION=";
        output += std::to_string(m_versionMaj);
        output +=  ".";
        output += std::to_string(m_versionMin);
    }

    //Now we want to encrypt the output before saving it.

    //We convert the entire output into binary
    for (unsigned int cnt = 0; cnt < output.size(); cnt++)
    {
        buffer.push_back(std::bitset<8>(output.c_str()[cnt]));
    }

    //Now we simply encrypt it and save it
    // We have to decide how to encrypt and decrypt it ...

    EncryptKeyRings(buffer, _kr);

    //For now, we will simply save it
    if (saveFile)
    {
        //Now, we start writing
        for (unsigned int cnt = 0; cnt < buffer.size(); cnt++)
        {
            saveFile.seekp(cnt);
            saveFile.write(reinterpret_cast<char*>(&buffer[cnt]),1);
        }

        saveFile.close();
    }
    else
    {
        std::cout << "Unable to open save location!" << std::endl;
    }
}

void KeyRingManager::LoadSettingsFile()
{
    std::ifstream file (m_settingsFilePath);

    if (file)
    {
        std::vector<std::string> sets;
        std::string tempStr;

        //Load entire file
        while (file >> tempStr)
        {
            if (tempStr == "-")
                tempStr = "";

            sets.push_back(tempStr);
        }

        //For now, we just hard-code
        //We want to avoid try-catches
        if (sets.size() < 6)
        {
            std::cout << "[CRITICAL ERROR]: Corrupted settings file!" << std::endl;
        }
        else
        {
            m_versionMaj = std::stoi(sets[0]);
            m_versionMin = std::stoi(sets[1]);
            m_defaultKeyRepPlugin = sets[2];
            m_useKeyRingPlugin = (bool)std::stoi(sets[3]);
            m_keyRep = sets[4];
            m_defaultSaveFilePath = sets[5];
            m_defaultLoadFilePath = sets[6];

        }

        file.close();
    }
    else
    {
        std::cout << "[ERROR]: Unable to load settings file!" << std::endl;
    }
}

void KeyRingManager::SaveSettingsFile()
{
    std::ofstream file (m_settingsFilePath);

    if (file)
    {
        file << m_versionMaj << "\n";
        file << m_versionMin << "\n";
        file << m_defaultKeyRepPlugin << "\n";
        file << m_useKeyRingPlugin << "\n";
        file << m_keyRep << "\n";
        if (m_defaultSaveFilePath == "")
        {
            file << "-" << "\n";
        }
        else
        {
            file << m_defaultSaveFilePath << "\n";
        }

        if (m_defaultLoadFilePath == "")
        {
            file << "-" << "\n";
        }
        else
        {
            file << m_defaultLoadFilePath << "\n";
        }

        file.close();
    }
    else
    {
        std::cout << "[ERROR]: Unable to save settings file!" << std::endl;
    }
}

void KeyRingManager::CreateNewKeyRing()
{
    std::string name, temp, encType = "";
    std::vector<std::string> passwords;
    KeyRing kr;

    //For now, this will be done via console input
    //Name
    std::cout << "Creating a new Key Ring..." << std::endl << "Enter name: ";
    std::cin >> name;

    //Encryption type
    while (encType == "")
    {
        std::cout << "Select encryption type: (Choose one of the following)" << std::endl;
        for (auto& itr : s_pluginsList)
        {
            if (itr.first != "ERROR")
                std::cout << "-> " << itr.first << std::endl;
        }
        std::cin >> encType;
        if (encType == "ERROR" || s_pluginsList.find(encType) == s_pluginsList.end())
        {
            encType = "";
        }
    }

    //Passwords
    for (unsigned int cnt = 0; cnt < 6; cnt++)
    {
        while (temp == "")
        {
            std::cout << "Enter password " << cnt << ": ";
            std::cin >> temp;

            //TODO: Add other conditions on password here (if needed)
            if (temp == "-" )
            {
                temp = "";
                std::cout << "[FAILED]: Invalid password. Please just mash keyboard if you wish to leave this 'blank'." << std::endl;
            }
            else
            {
                passwords.push_back(temp);
                temp = "";
                break;
            }
        }

    }

    //Save
    kr.SetName(name);
    kr.SetHash(GenerateHash(64));
    kr.SetEncryptionType(encType);
    kr.SetPassword0(passwords[0]);
    kr.SetPassword1(passwords[1]);
    kr.SetPassword2(passwords[2]);
    kr.SetPassword3(passwords[3]);
    kr.SetPassword4(passwords[4]);
    kr.SetPassword5(passwords[5]);

    m_keyRingsList.insert(std::pair<std::string, KeyRing>(kr.GetHash(), kr));
}

void KeyRingManager::DeleteKeyRing(std::string _hash)
{
    if (m_keyRingsList.find(_hash) == m_keyRingsList.end())
    {
        //Hash does not exist .. basically done
        std::cout << "[WARNING]: Hash did not exist in key rings list." << std::endl;
    }
    else
    {
        //We want to remove it from the map
        m_keyRingsList.erase(_hash);
    }
}

void KeyRingManager::ModifyKeyRing(std::string _hash)
{
    char attr = '^';
    std::string mod, encType = "";

    std::cout << "Editing Key Ring: " << std::endl;

    if (m_keyRingsList.find(_hash) == m_keyRingsList.end())
    {
        //Hash does not exist .. basically done
        std::cout << "[WARNING]: Hash did not exist in key rings list." << std::endl;
    }
    else
    {
        while (attr == '^')
        {
            m_keyRingsList.find(_hash)->second.Serialize();
            std::cout << "Select the attribute to be modified: " << std::endl;
            std::cout << "-> [N]ame" << std::endl << "-> [E]ncryption Type" << std::endl;
            std::cout << "-> Password [0]" << std::endl;
            std::cout << "-> Password [1]" << std::endl;
            std::cout << "-> Password [2]" << std::endl;
            std::cout << "-> Password [3]" << std::endl;
            std::cout << "-> Password [4]" << std::endl;
            std::cout << "-> Password [5]" << std::endl;
            std::cout << "-> [Q]uit" << std::endl;

            std::cout << "Choice: ";
            std::cin >> attr;

            if (attr == 'Q' || attr == 'q')
            {
                std::cout << "Quiting without making any changes ... " << std::endl;
                break;
            }

            //TODO: Modify input to possibly allow spaces for key ring names (?)
            std::cout << "Enter the new value (you may not use spaces): ";
            std::cin >> mod;

            switch(attr)
            {
            case 'N':
            case 'n':
            {
                m_keyRingsList.find(_hash)->second.SetName(mod);
                break;
            }
            case 'E':
            case 'e':
            {
                //Encryption type
                while (encType == "")
                {
                    std::cout << "Select encryption type: (Choose one of the following)" << std::endl;
                    for (auto& itr : s_pluginsList)
                    {
                        if (itr.first != "ERROR")
                            std::cout << "-> " << itr.first << std::endl;
                    }
                    std::cin >> encType;
                    if (encType == "ERROR" || s_pluginsList.find(encType) == s_pluginsList.end())
                    {
                        encType = "";
                    }
                }
                break;
            }
            case '0':
            {
                m_keyRingsList.find(_hash)->second.SetPassword0(mod);
                break;
            }
            case '1':
            {
                m_keyRingsList.find(_hash)->second.SetPassword1(mod);
                break;
            }
            case '2':
            {
                m_keyRingsList.find(_hash)->second.SetPassword2(mod);
                break;
            }
            case '3':
            {
                m_keyRingsList.find(_hash)->second.SetPassword3(mod);
                break;
            }
            case '4':
            {
                m_keyRingsList.find(_hash)->second.SetPassword4(mod);
                break;
            }
            case '5':
            {
                m_keyRingsList.find(_hash)->second.SetPassword5(mod);
                break;
            }
            default:
            {
                std::cout << "Invalid input! Please try again." << std::endl;
                attr = '^';
                break;
            }
            }
        }
    }
}

//These are used onlyfor keyring files
void KeyRingManager::EncryptKeyRings(std::vector<std::bitset<8>>& _data, KeyRing _kr)
{
    s_pluginsList.find(_kr.GetEncryptionType())->second->HandleDataENC(_kr.SerializeToList(), _data);
}

void KeyRingManager::DecryptKeyRings(std::vector<std::bitset<8>>& _data, KeyRing _kr)
{
    s_pluginsList.find(_kr.GetEncryptionType())->second->HandleDataDEC(_kr.SerializeToList(), _data);
}

std::string KeyRingManager::GenerateHash(unsigned int _size)
{
    const std::string alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    std::string hashStr = "";

    //May have to be careful not to call this function too frequently or the RNG may not have time to change seeds,
    //TODO: Better system for seeding RNG for Hash
    srand(time(NULL));

    //We will reseed the RNG, since doing so does not compromise the integrity of the passwords
    for (unsigned int cnt = 0; cnt < _size; cnt++)
    {
        hashStr += alpha[rand() % alpha.size()];
    }

    return hashStr;
}

std::string KeyRingManager::FetchHashByName(std::string _name)
{
    //NOTE: Be aware that FetchHashByName returns ONLY the FIRST occurance of _name if there are multiple
    for (auto& itr : m_keyRingsList)
    {
        if (itr.second.GetName() == _name)
            return itr.first;
    }

    std::cout << "[WARNING]: No Key Ring exists by the name " << _name << "!" << std::endl;
    return "keydoesnotexist";
}

std::string KeyRingManager::ListKeyRings()
{
    std::string ret;
    ret = "Available Key Rings: \n";
    for (auto itr : m_keyRingsList)
    {
        ret += "[" + itr.second.GetName() + "]: " + itr.second.GetEncryptionType() + "\n";
    }
    return ret;
}

std::string KeyRingManager::ListLoadedPlugins()
{
    std::string ret = "";
    for (auto itr : s_pluginsList)
    {
        if (itr.first != "ERROR")
        {
            if (itr.second == nullptr)
            {
                ret += "[FAILED]: ";
            }
            else
            {
                ret += "[LOADED]: ";
            }

            ret += itr.first;
            ret += "\n";
        }
    }
    return ret;
}

bool KeyRingManager::UseKeyRingPlugin()
{
    return m_useKeyRingPlugin;
}

void KeyRingManager::SetUseKeyRingPlugin(bool _new)
{
    m_useKeyRingPlugin = _new;
}

std::string KeyRingManager::GetDefaultKeyRepPlugin()
{
    return m_defaultKeyRepPlugin;
}

void KeyRingManager::SetDefaultKeyRepPlugin(std::string _new)
{
    m_defaultKeyRepPlugin = _new;
}

std::string KeyRingManager::GetDefaultSaveFilePath()
{
    return m_defaultSaveFilePath;
}

void KeyRingManager::SetDefaultSaveFilePath(std::string _newPath)
{
    m_defaultSaveFilePath = _newPath;
}

std::string KeyRingManager::GetDefaultLoadFilePath()
{
    return m_defaultLoadFilePath;
}

void KeyRingManager::SetDefaultLoadFilePath(std::string _newPath)
{
    m_defaultLoadFilePath = _newPath;
}

std::string& KeyRingManager::GetKeyRepLocation()
{
    return m_keyRep;
}

KeyRing& KeyRingManager::GetGlobalLock()
{
    return m_globalLock;
}
