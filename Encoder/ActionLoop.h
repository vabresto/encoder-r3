#ifndef ACTIONLOOP_H_INCLUDED
#define ACTIONLOOP_H_INCLUDED

#include "KeyRingManager.h"

extern char userInput;
extern std::string inp;
extern KeyRingManager krm;
extern const char * openFileName;
extern const char * saveFileName;
extern const char* filters[1];
extern bool selectedValidPluginName;
extern bool selectedValidName;
extern std::string pluginName;
extern std::string working;
extern bool hardQuit;

bool actionLoop();

void Run();

#endif // ACTIONLOOP_H_INCLUDED
