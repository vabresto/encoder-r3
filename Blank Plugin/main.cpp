#include "Plugin.h"
#include "KeyRing.h"
#include <memory>
#include <vector>
#include <string>
#include <bitset>
#include <random>
#include <functional>

class YourPlugin : public Plugin
{
private:
    //TODO: Add any private variables or functions you may need here

public:
    virtual std::string Command(std::string command, std::string options)
    {
        std::string temp;

        temp = "Received command: ";
        temp += command;
        temp += " with options: ";
        temp += options;

        return temp;
    }

    virtual std::string GetPluginName()
    {
        return "Your-Plugin-Name-Here";
    }

    virtual std::vector<std::string> RecommendedUses()
    {
        std::vector<std::string> vec;
        vec.push_back("Your-Recommended-Use-Here");
        return vec;
    }

    virtual void HandleDataENC(std::vector<std::string> _kr, std::vector< std::bitset<8> >& _data)
    {
        //TODO: Add your BULK ENCRYPTION code here
        return;
    }

    virtual void HandleDataDEC(std::vector<std::string> _kr, std::vector< std::bitset<8> >& _data)
    {
        //TODO: Add your BULK DECRYPTION code here
        return;
    }

    virtual void HandleByteENC(char& _byte)
    {
        //TODO: Add your BYTE ENCRYPTION code here
        return;
    }

    virtual void HandleByteDEC(char& _byte)
    {
        //TODO: Add your BYTE DECRYPTION code here
        return;
    }

    virtual void InitializeByteStream(std::vector<std::string> _kr)
    {
        //TODO: Initialize your plugin here (only called before one of the BYTE functions are called
        // Do not rely on this being called
    }
};

//TODO: Replace all occurances of 'YourPlugin' with the name of your plugin
//NOTE: The numbers here are your plugin's version, and the expected version of the main app
// These are not really used for anything right now
DEFINE_PLUGIN(YourPlugin, "YourPlugin", "1", "1")
