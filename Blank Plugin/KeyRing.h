#pragma once

#include <string>
#include <vector>

class KeyRing
{
//We are removing the encryption type enum, and instead, we will use std::string objects to identify cyphers
/*
public:
    //Enum to save what our KeyRing does
    enum EncryptionType
    {
        RNCypher, RSAEncrypt, RSADecrypt, ERROR
    };
*/
private:

    //Display name of the KeyRing (for the user)
    std::string m_name;

    //'Unique' ID for the KeyRing (when
    // the encrypted file is shared with other users, for example)
    std::string m_hash;

    //Encryption type
    std::string m_encryptionType;

    //Passwords to be used by the KeyRing - not all will
    //be used by all systems
    std::string m_password0;
    std::string m_password1;
    std::string m_password2;
    std::string m_password3;
    std::string m_password4;
    std::string m_password5;


public:
    //Constructor and destructor
    //We are going to leave these blank for now
    KeyRing();
    ~KeyRing();

    //Allow the user to change the KeyRing's name
    void SetName(std::string _name);
    std::string GetName();

    //Hash
    void SetHash(std::string _hash);
    std::string GetHash();

    //We will need to access what type of encryption this KeyRing does
    void SetEncryptionType(std::string _type);
    std::string GetEncryptionType();

    //We want to be able to set the passwords externally,
    //but it doesn't make sense to retrieve them outside this class
    void SetPassword0(std::string _new);
    std::string GetPassword0();
    void SetPassword1(std::string _new);
    std::string GetPassword1();
    void SetPassword2(std::string _new);
    std::string GetPassword2();
    void SetPassword3(std::string _new);
    std::string GetPassword3();
    void SetPassword4(std::string _new);
    std::string GetPassword4();
    void SetPassword5(std::string _new);
    std::string GetPassword5();

    std::string Serialize();
    std::string SerializeEncryptionType();

    std::vector<std::string> SerializeToList();
};
